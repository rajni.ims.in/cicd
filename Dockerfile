FROM node:12.7.0-alpine


# Install app dependencies
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .
ENTRYPOINT ["node","demo.js"]